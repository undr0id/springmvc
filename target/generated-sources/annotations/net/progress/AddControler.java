package net.progress;

import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import net.progress.models.ArithmeticOperations;

@Controller
public class AddControler {
	@RequestMapping(value = "/add")
	public ModelAndView add(@RequestParam("t1") int firstNumber, @RequestParam("t2") int secondNumber, ModelAndView mv) {
//		int firstNumber=Integer.parseInt(request.getParameter("t1"));
//		int secondNumber=Integer.parseInt(request.getParameter("t2"));

		int result=new ArithmeticOperations().adding(firstNumber, secondNumber);
		
		mv.setViewName("result");
		mv.addObject("adding", result);
		
		return mv;
	}
	
	@RequestMapping(value = "/add", params="subtract")
	public ModelAndView subtract(@RequestParam("t1") int firstNumber, @RequestParam("t2") int secondNumber, ModelAndView mv) {
//		int firstNumber=Integer.parseInt(request.getParameter("t1"));
//		int secondNumber=Integer.parseInt(request.getParameter("t2"));

		int result=new ArithmeticOperations().subtracting(firstNumber, secondNumber);
		
		mv.setViewName("result");
		mv.addObject("adding", result);
		
		return mv;
	}
}