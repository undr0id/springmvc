package net.progress;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class StudentController {
	@RequestMapping("/student")
	public ModelAndView getStudent() {
		//Move to a model!!!
		Student st=new Student();
		st.setName("Ivan Ivanov");
		st.setAge(35);
		st.setId(132455455);
		
		ModelAndView mv=new ModelAndView();
		mv.setViewName("student");
		mv.addObject("student", st);
		
		return mv;
	}
}
