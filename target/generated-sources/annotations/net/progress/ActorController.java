package net.progress;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javassist.CtBehavior;
import net.progress.databindings.Actor;
import net.progress.models.ActorOperations;

@Controller
public class ActorController {
	@RequestMapping("/actor/display")
	public ModelAndView getActor(HttpServletRequest request) {
		int id;
		if(request.getParameter("id")==null || "".equals(request.getParameter("id"))) {
			id = 1;
		}else {
			id = Integer.parseInt(request.getParameter("id"));
		}
		Actor actor=new ActorOperations().getActor(id);
		
		ModelAndView result=new ModelAndView();
		result.setViewName("actor/display");
		result.addObject("actor", actor);
		
		return result;
	}
	
	@RequestMapping(value="/actor/crud", params = "insert")
	public String insert(HttpServletRequest request) {
		Actor actor=new ActorOperations().insert(request);
		
		return "redirect: displayAll";
	}
	
	@RequestMapping(value="/actor/crud", params = "delete")
	public String delete(HttpServletRequest request) {
		new ActorOperations().delete(Integer.parseInt(request.getParameter("id")));

		return "redirect: displayAll";
	}
	
	@RequestMapping(value="/actor/crud", params = "update" )
	public String update(HttpServletRequest request) {	
		new ActorOperations().update(request);
		
		return "redirect: displayAll";
	}
	
	@RequestMapping("/actor/displayAll")
	public ModelAndView getAllActors() {
		List<Actor> actors = new ActorOperations().getAllActors();
		
		ModelAndView result=new ModelAndView();
		result.setViewName("actor/displayAll");
		result.addObject("actorsKey", actors);
		
		return result;
	}
}