package net.progress.models;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import net.progress.databindings.Actor;

public class ActorOperations {
	public Actor getActor(int id) {
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("pu");
		EntityManager em=emf.createEntityManager();
		Session session=em.unwrap(Session.class);
		
		Actor actor=session.get(Actor.class, id);
		session.close();
		
		return actor;
	}
	
	public Actor insert(HttpServletRequest request) {
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("pu");
		EntityManager em=emf.createEntityManager();
		Session session=em.unwrap(Session.class);
		Transaction transaction=session.beginTransaction();
		
		Actor actor=new Actor();
		actor.setFirstName(request.getParameter("firstName"));
		actor.setLastName(request.getParameter("lastName"));
		
		session.save(actor);
		transaction.commit();
		session.close();
		
		return actor;
	}

	public void delete(int id) {
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("pu");
		EntityManager em=emf.createEntityManager();
		Session session=em.unwrap(Session.class);
		Transaction transaction=session.beginTransaction();
		
		Actor actor=session.get(Actor.class, id);
		session.delete(actor);
		
		transaction.commit();
		session.close();
	}
	
	public void update(HttpServletRequest request) {
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("pu");
		EntityManager em=emf.createEntityManager();
		Session session=em.unwrap(Session.class);
		Transaction transaction=session.beginTransaction();
		
		Actor actor=new Actor();
		actor.setId(Integer.parseInt(request.getParameter("id")));
		actor.setFirstName(request.getParameter("firstName"));
		actor.setLastName(request.getParameter("lastName"));
		
		session.update(actor);
		transaction.commit();
		session.close();
	}
	
	public List<Actor> getAllActors(){
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("pu");
		EntityManager em=emf.createEntityManager();
		Session session = em.unwrap(Session.class);
		
		String hql = "FROM Actor";
		Query query = session.createQuery(hql);
		return query.list();
	}
}
